import Foto from '../img/selfie-sorteio.jpg'

const nomes = document.querySelectorAll('.nome__aluno');
const botaoSortear = document.getElementById('sortear');

const wrapperSelecionado = document.getElementById('lista__nomes');
const listaNomes = document.getElementById('nome__scroll');

const selecionado = document.getElementById('selecionado');
let selecionadoFim = {
  tag: 'p',
  infos: selecionado.innerText,
  classe: 'selecionado',
}

const ganhador = document.createElement(selecionadoFim.tag);
const fotoGanhador = document.createElement('img');
fotoGanhador.setAttribute('src', Foto);
fotoGanhador.classList.add('imagem__ganhador')


/* sortear o nome */
botaoSortear.addEventListener('click', () => {

  if (botaoSortear.disabled) {
    document.querySelector('.ganhador__escolhido').classList.add('visivel');
  }

  let tempoTransicao = 5 / nomes.length * 1000;
  let tempoScroll = 5 / nomes.length * 1000;
  let top = 46.8;
  wrapperSelecionado.scrollTop = 0;
  botaoSortear.disabled = true;
  botaoSortear.remove();

  for (let nome of nomes) {
    setTimeout(() => {
      nome.classList.add('ativo');

      setTimeout(() => {
        nome.classList.remove('ativo');

        /* scroll na lista de nomes */
        if (getComputedStyle(listaNomes).bottom.slice(0,-2) < listaNomes.scrollHeight) {
          setTimeout(() => {
            listaNomes.style.bottom = `${top}px`
            top += 46.8;
          }, 20);

          /* adicionar o ganhador ao final */
          if (nome === nomes[nomes.length-3]) {
            ganhador.classList.add(`${selecionadoFim.classe}`);
            ganhador.innerText = selecionadoFim.infos;

            wrapperSelecionado.appendChild(ganhador);
            wrapperSelecionado.appendChild(fotoGanhador);
            wrapperSelecionado.scrollTop = wrapperSelecionado.scrollHeight;

            setTimeout(() => {
              ganhador.classList.add('visivel');
              fotoGanhador.classList.add('visivel');
            }, 50);
          }
        }
      }, tempoScroll * 1.4);
    }, tempoTransicao);

    tempoTransicao += (5000 - tempoTransicao) / nomes.length;
  }
});

/* impedir o scroll depois do sorteio */
wrapperSelecionado.addEventListener('scroll', () => {
  if (
    ganhador.classList.contains('visivel') &&
    Number(wrapperSelecionado.scrollTop.toFixed()) >= Number(wrapperSelecionado.scrollHeight.toFixed()-1000)
  ) {
    wrapperSelecionado.scrollTop = wrapperSelecionado.scrollHeight;

  }
})
